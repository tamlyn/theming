import React from 'react'
import styled from 'styled-components'

export const Label = styled.label`
  display: flex;
`
export const Text = styled.span`
  flex: 50%;
`
export const Input = styled.input`
  flex: 50%;
`

export default ({ label, ...props }) => (
  <Label>
    {label && <Text>{label}</Text>}
    <Input {...props} />
  </Label>
)
