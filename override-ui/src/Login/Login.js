import React from 'react'
import styled from 'styled-components'

import Button from '../Button/Button'
import TextInput from '../TextInput/TextInput'

export const Container = styled.div``
export const Footer = styled.div``
export const ButtonWithSpacing = Button.extend``

export default () => (
  <Container>
    <TextInput label="Username" placeholder="Username..." />
    <TextInput label="Password" placeholder="Password..." type="password" />

    <Footer>
      <Button onClick={() => console.log('Hello')} primary>
        Login
      </Button>
      <ButtonWithSpacing>Cancel</ButtonWithSpacing>
    </Footer>
  </Container>
)
