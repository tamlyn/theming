import React from 'react'
import styled, { withTheme } from 'styled-components'

const DefaultLabel = styled.label`
  display: flex;
`
const DefaultText = styled.span`
  flex: 50%;
`
const DefaultInput = styled.input`
  flex: 50%;
`

const TextInput = ({
  theme: {
    TextInputLabel: Label = DefaultLabel,
    TextInputText: Text = DefaultText,
    TextInputInput: Input = DefaultInput,
  },
  label,
  ...props
}) => (
  <Label>
    {label && <Text>{label}</Text>}
    <Input {...props} />
  </Label>
)

export default withTheme(TextInput)
