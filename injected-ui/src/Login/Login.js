import React from 'react'
import styled, { withTheme } from 'styled-components'

import DefaultButton from '../Button/Button'
import DefaultTextInput from '../TextInput/TextInput'

const DefaultContainer = styled.div``
const DefaultFooter = styled.div``
const DefaultButtonWithSpacing = DefaultButton.extend`
  margin-left: 1em;
`

const Login = ({
  theme: {
    Button = DefaultButton,
    TextInput = DefaultTextInput,
    LoginContainer: Container = DefaultContainer,
    LoginFooter: Footer = DefaultFooter,
    LoginButtonWithSpacing: ButtonWithSpacing = DefaultButtonWithSpacing,
  },
}) => (
  <Container>
    <TextInput label="Username" placeholder="Username..." />
    <TextInput label="Password" placeholder="Password..." type="password" />

    <Footer>
      <Button onClick={() => console.log('Hello')} primary>
        Login
      </Button>
      <ButtonWithSpacing>Cancel</ButtonWithSpacing>
    </Footer>
  </Container>
)

export default withTheme(Login)
