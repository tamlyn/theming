import styled from 'styled-components'

import buttonStyles from './ui/Button'
import { Button } from 'override-ui'

import loginStyles from './ui/Login'
import {
  ButtonWithSpacing,
  Footer,
  Container,
} from 'override-ui/src/Login/Login'

import textInputStyles from './ui/TextInput'
import {Text, Input, Label} from 'override-ui/src/TextInput/TextInput'

export default styled.div`
  // Button
  ${Button}, ${ButtonWithSpacing} {
    ${buttonStyles};
  }

  // Login
  ${Container} {
    ${loginStyles.Container};
  }
  ${ButtonWithSpacing} {
    ${loginStyles.ButtonWithSpacing};
  }
  ${Footer} {
    ${loginStyles.Footer};
  }
  
  // TextInput
  ${Text} {
    ${textInputStyles.Text}
  }
  ${Input} {
    ${textInputStyles.Input}
  }
  ${Label} {
    ${textInputStyles.Label}
  }
`
