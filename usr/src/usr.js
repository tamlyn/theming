import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import mergeWith from 'lodash.mergeWith'
import { getContext, withProps, compose } from 'recompose'

export default (name, baseClasses) =>
  compose(
    getContext({ usrTheme: PropTypes.object }),
    withProps(({ usrTheme }) => ({
      theme: mergeWith({}, baseClasses, usrTheme[name], (base, theme) =>
        classnames(base, theme),
      ),
    })),
  )
