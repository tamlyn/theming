import React from 'react'
import PropTypes from 'prop-types'
import { withContext } from 'recompose'

const ThemeProvider = ({ children }) => children
const hoc = withContext({ usrTheme: PropTypes.object }, ({ theme }) => ({
  usrTheme: theme,
}))
export default hoc(ThemeProvider)
