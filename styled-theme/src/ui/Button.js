import { css } from 'styled-components'

export default css`
  border: 2px solid;
  border-radius: 0;
  background: white;
  ${props =>
    props.primary
      ? `border-color: ${props.theme.primaryColor};
         color: ${props.theme.primaryColor}`
      : ``};
`
