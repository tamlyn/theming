import { css } from 'styled-components'

export default {
  Container: css`
    padding: ${props => props.theme.baseSize};
    border: 2px solid black;
  `,
  Footer: css`
    text-align: center;
  `,
  ButtonWithSpacing: css`
    margin-left: ${props => props.theme.baseSize};
  `,
}
