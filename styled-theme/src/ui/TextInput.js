import { css } from 'styled-components'

export default {
  Label: css``,
  Text: css``,
  Input: css`
    border: 2px solid;
    margin-bottom: calc(${props => props.theme.baseSize} / 2);
  `,
}
