import * as ui from './ui'

export default {
  primaryColor: '#fab',
  baseSize: '16px',
  ui,
}
