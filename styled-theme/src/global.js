import { injectGlobal } from 'styled-components'

injectGlobal`
    body {
        font-family: sans-serif;
        line-height: 1.6;
    }
`
