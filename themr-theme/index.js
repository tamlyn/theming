import './src/variables.css'
import './src/global.css'

export default {
  Login: require('./src/ui/Login.css'),
  Button: require('./src/ui/Button.css'),
  TextInput: require('./src/ui/TextInput.css'),
}
