import React from 'react'
import { themr } from 'react-css-themr'

import Button from '../Button/Button'
import TextInput from '../TextInput/TextInput'
import styles from './Login.css'

const Login = ({ theme }) => (
  <div className={theme.root}>
    <TextInput label="Username" placeholder="Username..." />
    <TextInput label="Password" placeholder="Password..." type="password" />

    <div className={theme.footer}>
      <Button onClick={() => console.log('Hello')} primary>
        Login
      </Button>
      <Button>Cancel</Button>
    </div>
  </div>
)

export default themr('Login', styles)(Login)
