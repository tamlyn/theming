import React from 'react'
import styled from 'styled-components'

import Button from '../Button/Button'
import TextInput from '../TextInput/TextInput'

const Container = styled.div`
  ${props => props.theme.ui.Login.Container};
`
const Footer = styled.div`
  ${props => props.theme.ui.Login.Footer};
`
const ButtonWithSpacing = Button.extend`
  ${props => props.theme.ui.Login.ButtonWithSpacing};
`

export default () => (
  <Container>
    <TextInput label="Username" placeholder="Username..." />
    <TextInput label="Password" placeholder="Password..." type="password" />

    <Footer>
      <Button onClick={() => console.log('Hello')} primary>
        Login
      </Button>
      <ButtonWithSpacing>Cancel</ButtonWithSpacing>
    </Footer>
  </Container>
)
