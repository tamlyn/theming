import React from 'react'
import styled from 'styled-components'

const Label = styled.label`
  display: flex;
  ${props => props.theme.ui.TextInput.Label};
`
const Text = styled.span`
  flex: 50%;
  ${props => props.theme.ui.TextInput.Text};
`
const Input = styled.input`
  flex: 50%;
  ${props => props.theme.ui.TextInput.Input};
`

export default ({ label, ...props }) => (
  <Label>
    {label && <Text>{label}</Text>}
    <Input {...props} />
  </Label>
)
