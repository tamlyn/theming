import styled from 'styled-components'

export default styled.button`
  ${props => props.theme.ui.Button};
`
