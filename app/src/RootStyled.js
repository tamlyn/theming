import React from 'react'
import { ThemeProvider } from 'styled-components'

import theme from 'styled-theme'
import { Login } from 'styled-ui'

export default () => (
  <ThemeProvider theme={theme}>
    <Login />
  </ThemeProvider>
)
