import React from 'react'
import { ThemeProvider } from 'styled-components'

import theme from 'injected-theme'
import { Login } from 'injected-ui'

export default () => (
  <ThemeProvider theme={theme}>
    <Login />
  </ThemeProvider>
)
