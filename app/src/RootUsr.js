import React from 'react'
import { ThemeProvider } from 'usr'

import theme from 'usr-theme'
import { Login } from 'usr-ui'

export default () => (
    <ThemeProvider theme={theme}>
      <Login />
    </ThemeProvider>
)
