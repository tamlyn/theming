import React from 'react'
import { ThemeProvider } from 'react-css-themr'

import theme from 'themr-theme'
import { Login } from 'themr-ui'

export default () => (
  <ThemeProvider theme={theme}>
    <Login />
  </ThemeProvider>
)
