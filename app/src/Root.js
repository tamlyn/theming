import React from 'react'
import RootStyled from './RootStyled'
import RootThemr from './RootThemr'
import RootInjected from './RootInjected'
import RootOverride from "./RootOverride"
import RootUsr from "./RootUsr"

export default () => (
  <div>
    <h2>React CSS Themr</h2>
    <RootThemr />

    <h2>Styled Components (props)</h2>
    <RootStyled />

    <h2>Styled Components (wrapper)</h2>
    <RootOverride />

    <h2>Injected Components</h2>
    <RootInjected />

    <h2>React Usr</h2>
    <RootUsr />
  </div>
)
