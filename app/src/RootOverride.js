import React from 'react'
import { ThemeProvider } from 'styled-components'

import { theme, Wrapper } from 'override-theme'
import { Login } from 'override-ui'

export default () => (
  <ThemeProvider theme={theme}>
    <Wrapper>
      <Login />
    </Wrapper>
  </ThemeProvider>
)
