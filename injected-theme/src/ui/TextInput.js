import styled from 'styled-components'

export const Label = styled.label`
  display: flex;
`

export const Text = styled.span`
  flex: 50%;
`

export const Input = styled.input`
  flex: 50%;
  border: 2px solid;
  margin-bottom: calc(${props => props.theme.baseSize} / 2);
`
