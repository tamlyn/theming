import styled from 'styled-components'
import Button from './Button'

export const Container = styled.div`
  padding: ${props => props.theme.baseSize};
  border: 2px solid black;
`
export const Footer = styled.div`
  text-align: center;
`
export const ButtonWithSpacing = Button.extend`
  margin-left: ${props => props.theme.baseSize};
`
