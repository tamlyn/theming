import styled from 'styled-components'

export default styled.button`
  border: 2px solid;
  border-radius: 0;
  background: white;
  ${props =>
    props.primary
      ? `border-color: ${props.theme.primaryColor};
         color: ${props.theme.primaryColor}`
      : ``};
`
