# Theming comparison

Realistic usage examples of React CSS Themr and Styled Components

## Setup

    yarn
    npm start

Visit the URL printed in the terminal, usually <http://localhost:8080>.

## Guide

* React CSS Themr

  * `themr-ui` is the component library. Each component includes a CSS file
    which can include base styles.
  * `themr-theme` is the theme which includes both CSS variables and the bulk of
    styles for individual components.

* Styled Components (props)

  * `styled-ui` is the component library. Each component can have base styles
    and also injects a string of arbitrary CSS provided by the theme.
  * `styled-theme` is the theme which defines variables as well as CSS
    overrides.

* Styled Components (wrapper)

  * `override-ui` is the component library. Each component can have base styles
    and is exported so it can be targeted by the theme.
  * `override-theme` is the theme which defines variables as a `Wrapper`
    component which applies CSS overrides.

* Injected Components

  * `injected-ui` is the component library. Each component has built in styles
    but also allows tis child components to be changed based on the theme.
  * `injected-theme` is the theme which provides variables and full styled
    components to override styles.

* `app` is the application which renders both `Login` components and applies the
  themes.
