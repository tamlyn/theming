import React from 'react'
import { usr } from 'usr'
import classnames from 'classnames'

import style from './Button.css'

const Button = ({ theme, children, primary, ...props }) => (
  <button
    className={classnames(theme.root, primary && theme.primary)}
    {...props}
  >
    {children}
  </button>
)

export default usr('Button', style)(Button)
