import React from 'react'
import { usr } from 'usr'

import style from './TextInput.css'

const TextInput = ({ theme, label, ...props }) => (
  <label className={theme.root}>
    {label && <span className={theme.label}>{label}</span>}
    <input className={theme.input} {...props} />
  </label>
)

export default usr('TextInput', style)(TextInput)
